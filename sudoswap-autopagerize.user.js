// ==UserScript==
// @name         Sudoswap AutoPagerize
// @namespace    https://kudasaijp.com/
// @version      0.1
// @description  Adds AutoPager to Sudoswap
// @author       Nakochi
// @license      MIT License
// @match        https://sudoswap.xyz/*
// @run-at       document-idle
// @grant        none
// ==/UserScript==

(() => {
  'use strict';

  const $ = document.querySelector.bind(document);

  const autopager = new IntersectionObserver(([entry]) => {
    if (entry.isIntersecting) {
      entry.target.click();
    }
  });

  new MutationObserver((mutations, self) => {
    const btn = $('.loadMoreBtn');
    if (btn) {
      autopager.observe(btn);
      self.disconnect();
    }
  }).observe($('app-root'), { childList: true, subtree: true });
})();
