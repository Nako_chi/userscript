// ==UserScript==
// @name         DEFY Pledged FCoin Checker
// @namespace    https://kudasaijp.com/
// @version      0.1
// @description  DEFY FCoin kudasai
// @author       Nakochi
// @license      MIT License
// @match        https://opensea.io/collection/defy-genesis-masks*
// @run-at       document-idle
// @grant        none
// ==/UserScript==

(async () => {
  'use strict';

  const PledgedFCoin = {
    KEY: 'defy-pledged-fcoin',
    URL: 'https://webapi.prod.defylabs.xyz/Leaderboards/pledgedFCoin',
    cache: undefined,
    async getCache() {
      if (this.cache) return this.cache;
      const storage = sessionStorage.getItem(this.KEY);
      if (storage) {
        this.cache = JSON.parse(storage);
        return this.cache;
      }
      this.cache = await this.fetchLeaderboards();
      sessionStorage.setItem(this.KEY, JSON.stringify(this.cache));
      return this.cache;
    },
    async getValue(tokenId) {
      const cache = await this.getCache();
      return cache[tokenId] ?? 0;
    },
    async fetchLeaderboards() {
      const array = [];
      const leaderboards = await fetch(this.URL).then((res) => res.json());
      for (const { displayName, value } of leaderboards) {
        const [, tokenId] = displayName.match(/#(\d+)$/);
        array[Number(tokenId)] = value;
      }
      return array;
    }
  }

  async function addPledgedValue(element) {
    const name = element.querySelector('.AssetCardFooter--name');
    const [, tokenId] = name.textContent.match(/#(\d+)$/) || [];
    const value = await PledgedFCoin.getValue(Number(tokenId));
    const div = document.createElement('div');
    div.classList.add('AssetCardFooter--name');
    div.textContent = value + ' Pledged';
    name.after(div);
  }

  document.querySelectorAll('[role="gridcell"]').forEach(addPledgedValue);

  new MutationObserver((mutations) => {
    for (const mutation of mutations) {
      if (mutation.addedNodes.length) {
        addPledgedValue(mutation.addedNodes[0]);
      }
    }
  }).observe(
    document.querySelector('[role="grid"]'),
    { childList: true }
  );
})();
