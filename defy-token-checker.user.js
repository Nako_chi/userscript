// ==UserScript==
// @name         DEFY Token Checker
// @namespace    https://kudasaijp.com/
// @version      0.1
// @description  DEFY Token kudasai
// @author       Nakochi
// @license      MIT License
// @match        https://opensea.io/collection/defy-genesis-masks
// @run-at       document-idle
// @grant        none
// ==/UserScript==

(() => {
  'use strict';

  const cache = new Map();

  async function call(tokenId) {
    if (cache.has(tokenId)) return cache.get(tokenId);

    const hex = tokenId.toString(16);
    const data = '0xd43500370000000000000000000000000000000000000000000000000000000000000000';
    const res = await fetch('https://polygon-rpc.com/', {
      method: 'POST',
      body: JSON.stringify({
        jsonrpc: '2.0',
        id: 3,
        method: 'eth_call',
        params: [
          {
            from: '0x0000000000000000000000000000000000000000',
            data: data.slice(0, -hex.length) + hex,
            to: '0xfd257ddf743da7395470df9a0517a2dfbf66d156',
          },
          'latest',
        ]},
      ),
    });
    const json = await res.json();
    const tokens = Number(json.result);

    cache.set(tokenId, tokens);
    return tokens;
  }

  async function addTokens(element) {
    const name = element.querySelector('.AssetCardFooter--name');
    const [, tokenId] = name.textContent.match(/#(\d+)$/) || [];
    const tokens = await call(Number(tokenId));
    const div = document.createElement('div');
    div.classList.add('AssetCardFooter--name');
    div.textContent = tokens + ' DEFY';
    name.after(div);
  }

  document.querySelectorAll('[role="gridcell"]').forEach(addTokens);

  new MutationObserver((mutations) => {
    for (const mutation of mutations) {
      if (mutation.addedNodes.length) {
        addTokens(mutation.addedNodes[0]);
      }
    }
  }).observe(
    document.querySelector('[role="grid"]'),
    { childList: true }
  );
})();
